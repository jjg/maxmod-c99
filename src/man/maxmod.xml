<?xml version="1.0" encoding="UTF-8"?>
<refentry
    id='maxmod3'
    xmlns='http://docbook.org/ns/docbook'
    version='5.0'
    xml:lang='en'
    xmlns:xi='http://www.w3.org/2003/XInclude'
    xmlns:xlink='http://www.w3.org/1999/xlink'>

<xi:include href="refentryinfo-date.xml"/>

<refmeta>
  <refentrytitle>MAXMOD</refentrytitle>
  <manvolnum>3</manvolnum>
  <refmiscinfo class='manual'>Library Functions Manual</refmiscinfo>
</refmeta>
<xi:include href="refmeta.xml"/>

<refnamediv id='name'>
  <refname>maxmod</refname>
  <refpurpose>
    find the maximum modulus of a complex polynomial on the unit disc.
  </refpurpose>
</refnamediv>

<refsynopsisdiv id='synopsis'>

<funcsynopsis>

<funcsynopsisinfo>
#include &lt;maxmod.h&gt;
</funcsynopsisinfo>

<funcprototype>
  <?dbhtml funcsynopsis-style='ansi'?>
  <funcdef>extern int <function>maxmod_d</function></funcdef>
  <paramdef>const double complex *<parameter>coeffs</parameter></paramdef>
  <paramdef>size_t <parameter>order</parameter></paramdef>
  <paramdef>maxmod_opt_t <parameter>options</parameter></paramdef>
  <paramdef>maxmod_results_t *<parameter>results</parameter></paramdef>
</funcprototype>

<funcprototype>
  <?dbhtml funcsynopsis-style='ansi'?>
  <funcdef>extern double <function>maxmod</function></funcdef>
  <paramdef>const double complex *<parameter>coeffs</parameter></paramdef>
  <paramdef>size_t <parameter>order</parameter></paramdef>
</funcprototype>

</funcsynopsis>

</refsynopsisdiv>

<refsect1 id='description'><title>DESCRIPTION</title>

<para>
  The <function>maxmod</function> and <function>maxmod_d</function>
  functions calculate the maximum modulus of a complex polynomial
  on the unit disc. The <function>maxmod</function> function offers
  a simplified interface while <function>maxmod_d</function> gives
  access to a detailed interface.
</para>

<para>
  For both functions the first two parameters are the
  <varname>coefficients</varname> and <varname>order</varname>
  of the polynomial. Thus the <varname>coefficients</varname>
  varname is an array of <varname>order</varname> + 1 complex
  values.
</para>

<para>
  For the <function>maxmod_d</function> function the third parameter
  is a structure <structname>maxmod_opt_t</structname> discussed below.
  The final argument is a pointer to a
  <structname>maxmod_results_t</structname> structure.
</para>

</refsect1>

<refsect1 id='options'><title>OPTIONS</title>

<para>
  The detailed interface uses a
  <structname>maxmod_opt_t</structname> structure to
  pass options to the function. The fields are:
</para>

<variablelist>

<varlistentry>
  <term>
    <type>double</type>
    <structfield>relerror</structfield>
  </term>
  <listitem>
    <para>
      The required relative accuracy in the result;
    </para>
  </listitem>
</varlistentry>

<varlistentry>
  <term>
    <type>size_t</type>
    <structfield>max.evaluations</structfield>
  </term>
  <listitem>
    <para>
      The maximum number of polynomial evaluations to be used
      in calculating the maximum modulus. If this number is
      exceeded then the function terminates early and returns
      <symbol>MAXMOD_INACCURATE</symbol>;
    </para>
    <para>
      Use a value of zero for unlimited evaluations.
    </para>
  </listitem>
</varlistentry>

<varlistentry>
  <term>
    <type>size_t</type>
    <structfield>max.intervals</structfield>
  </term>
  <listitem>
    <para>
      An upper bound on the size of the interval table, behaving
      as <structfield>max.evaluations</structfield>.
    </para>
  </listitem>
</varlistentry>

</variablelist>

</refsect1>

<refsect1 id='results'><title>RESULTS</title>

<para>
  The detailed interface returns its results in the
  <structname>maxmod_results_t</structname> passed to the
  function by reference. The fields are:
</para>

<variablelist>

<varlistentry>
  <term>
    <type>double</type>
    <structfield>maxmod</structfield>
  </term>
  <listitem>
    <para>
      The maximum modulus;
    </para>
  </listitem>
</varlistentry>

<varlistentry>
  <term>
    <type>double</type>
    <structfield>relerror</structfield>
  </term>
  <listitem>
    <para>
      The relative error in the maximum modulus;
    </para>
  </listitem>
</varlistentry>

<varlistentry>
  <term>
    <type>double</type>
    <structfield>width</structfield>
  </term>
  <listitem>
    <para>
      The width of each interval at the end of the processing;
    </para>
  </listitem>
</varlistentry>

<varlistentry>
  <term>
    <type>size_t</type>
    <structfield>evaluations</structfield>
  </term>
  <listitem>
    <para>
      The number of polynomial evaluation used.
    </para>
  </listitem>
</varlistentry>

</variablelist>

</refsect1>

<refsect1 id='return_value'><title>RETURN VALUE</title>

<para>
  The simplified interface returns the estimate of the
  maximum modulus.
</para>

<para>
  The detailed interface return an integer errorcode
  defined by the symbols:
</para>

<variablelist>

<varlistentry>
  <term><symbol>MAXMOD_OK</symbol></term>
  <listitem>
    <para>
      Success;
    </para>
  </listitem>
</varlistentry>

<varlistentry>
  <term><symbol>MAXMOD_INACCURATE</symbol></term>
  <listitem>
    <para>
      The function terminated before the requested
      accuracy could be achieved. The results structure
      is modified to give the (inaccurate) estimate found;
    </para>
  </listitem>
</varlistentry>

<varlistentry>
  <term><symbol>MAXMOD_ERROR</symbol></term>
  <listitem>
    <para>
      An error occurred, the results structure is not
      modified.
    </para>
  </listitem>
</varlistentry>

</variablelist>

</refsect1>

<refsect1 id='accuracy'><title>ACCURACY</title>

<para>
  If the polynomial evaluation is exact then the algorithm
  should be able to obtain arbitrary accuracy. However, for floating
  point arithmetic the relative error in evaluating a polynomial
  is of the order of the relative error in representing a floating
  point value: around machine epsilon.
</para>

<para>
  If the user specifies a required relative accuracy which
  of the order (or smaller) than machine epsilon then, in the closing
  stages of the algorithm, the polynomial evaluates as constant.
  This can lead to an exponential explosion in the number of intervals
  used eventually exhausting memory.
</para>

<para>
  Consequently we would recommend the the relative accuracy
  requested is larger than machine epsilon by a factor of 100.
</para>

<para>
  If this advice is not followed then one can mitigate the
  effects of the explosion by setting the
  <structfield>max.evaluations</structfield> or
  <structfield>max.intervals</structfield> fields of the
  <structname>maxmod_opt_t</structname> structure.
</para>

</refsect1>

<refsect1 id='author'><title>AUTHOR</title>

<para>J.J. Green</para>

</refsect1>
</refentry>
