<?xml version='1.0'?>
<!-- docbook 5.0 customisations -->
<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'>
  <xsl:import href='http://docbook.sourceforge.net/release/xsl-ns/current/manpages/docbook.xsl'/>
  <!-- supresses 'notes' section with pointless HTML URLs -->
  <xsl:param name='man.endnotes.list.enabled'>0</xsl:param>
  <xsl:param name='man.endnotes.are.numbered'>0</xsl:param>
</xsl:stylesheet>
