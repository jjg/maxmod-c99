#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "poly.h"
#include "ivtable.h"
#include "round3.h"

#include <float.h>
#include <math.h>
#include <limits.h>
#include <stdbool.h>
#include <stdlib.h>

#define IVTABLE_EXPAND 3

typedef struct
{
  long mid;
  double value;
  bool retain;
} iv_t;

struct ivtable_t
{
  iv_t *iv;
  size_t total;
  size_t active;
  size_t evaluated;
  long width;
  long n_max;
  long N;
  double Delta;
};

/* read accessors */

size_t ivtable_evaluated(const ivtable_t *T)
{
  return T->evaluated;
}

size_t ivtable_total(const ivtable_t *T)
{
  return T->total;
}

size_t ivtable_active(const ivtable_t *T)
{
  return T->active;
}

long ivtable_width(const ivtable_t *T)
{
  return T->width;
}

double ivtable_delta(const ivtable_t *T)
{
  return T->Delta;
}

static int ivtable_expand(size_t, ivtable_t*);
static void ivtable_vars(double*, long*);
static int ivtable_compress(ivtable_t*);

ivtable_t* ivtable_new(void)
{
  ivtable_t *T = malloc(sizeof(ivtable_t));

  if (!T) return NULL;

  T->iv = NULL;
  T->total = 0;
  T->active = 0;
  T->evaluated = 0;
  T->width = 0;
  T->n_max = 0;
  T->Delta = 0.0;

  return T;
}

void ivtable_destroy(ivtable_t *T)
{
  if (T)
    free(T->iv);
  free(T);
}

/*
  note that the cast to double here rounds to a representable
  double, but whether that is upward or not is implementation
  defined (C99, sec. 6.3.1.4, para. 2), it is not affected by
  the floating point rounding mode.  This is somewhat academic
  in our application, the triadic exponent is unaffected for
  32 and 64 bit longs ...
*/

static void ivtable_vars(double *Delta, long *n_max)
{
  double
    abs_maxlong = fmax((double)LONG_MAX, -(double)LONG_MIN),
    triadic_exponent = floor(log(2 * abs_maxlong + 1) / log(3)),
    N = pow(3.0, triadic_exponent);

  *n_max = floor((N - 1) / 2);
  *Delta = 2 * M_PI / N;
}

int ivtable_init(size_t size, size_t initial, ivtable_t *T)
{
  if (size < (initial = round3(initial)))
    size = initial;

  iv_t *iv;

  if (T->iv)
    {
      if ((iv = realloc(T->iv, size * sizeof(iv_t))) == NULL)
	return 1;
    }
  else
    {
      if ((iv = malloc(size * sizeof(iv_t))) == NULL)
	return 1;
    }

  long n_max;
  double Delta;

  ivtable_vars(&Delta, &n_max);

  long width = 2 * ((n_max - ((initial - 1) / 2)) / initial) + 1;

  /* the cast here is required - initial is unsigned */

  long mid = width * ((1 - (long)initial) / 2);

  for (size_t i = 0 ; i < initial ; i++)
    {
      iv[i].mid = mid;
      mid += width;
    }

  T->iv = iv;
  T->total = size;
  T->active = initial;
  T->width = width;
  T->evaluated = 0;
  T->Delta = Delta;
  T->n_max = n_max;

  return 0;
}

static int ivtable_expand(size_t size, ivtable_t *T)
{
  if (size < 1)
    return 1;

  iv_t *iv = realloc(T->iv, size * sizeof(iv_t));

  if (iv == NULL)
    return 1;

  T->iv = iv;
  T->total = size;

  return 0;
}

int ivtable_eval(const double complex *p, size_t n,
                 ivtable_t* T, size_t maxevals)
{
  size_t active = T->active;
  double Delta = T->Delta;
  iv_t *iv = T->iv;

  if (maxevals && T->evaluated + active > maxevals)
    return 1;

  size_t i;

  for (i = T->evaluated ; i < active ; i++)
    {
      double t = iv[i].mid * Delta;
      double complex z = cos(t) + sin(t) * I;

      iv[i].value = poly_sqr_abs_eval(p, n, z);
    }

  T->evaluated = i;

  return 0;
}

double ivtable_largest(const ivtable_t *T)
{
  size_t evaluated = T->evaluated;
  iv_t *iv = T->iv;
  double current = 0, max = 0;

  for (size_t i = 0 ; i < evaluated ; i++)
    {
      current = iv[i].value;
      if (current > max)
	max = current;
    }

  return max;
}

int ivtable_reject(ivtable_t *T, double threshold, size_t *R)
{
  size_t
    rejected = 0,
    active = T->active;
  iv_t *iv = T->iv;

  for (size_t i = 0 ; i < active ; i++)
    {
      double current = iv[i].value;

      if (current >= threshold)
	iv[i].retain = true;
      else
	{
	  rejected++;
	  iv[i].retain = false;
	}
    }

  if (rejected)
    {
      if (ivtable_compress(T) > rejected)
	return 1;
    }

  if (R)
    *R = rejected;

  return 0;
}

int ivtable_compress(ivtable_t *T)
{
  iv_t *iv = T->iv;
  size_t
    bottom = 0,
    swapped = 0,
    top = T->active - 1;

  while (1)
    {
      while (iv[bottom].retain)
        bottom++;

      while (! iv[top].retain)
        top--;

      if (bottom > top)
	break;
      else
	{
	  iv_t tmp = iv[bottom];

	  iv[bottom] = iv[top];
	  iv[top] = tmp;

	  swapped++;
	}
    }

  T->active = bottom;
  T->evaluated = bottom;

  return swapped;
}

int ivtable_subdivide(ivtable_t *T, size_t maxsize)
{
  long
    active = T->active,
    width = T->width;

  if (width <= 1)
    return 1;
  else
    width /= 3;

  if (T->total <= 3 * active)
    {
      size_t newsize = active * IVTABLE_EXPAND;

      if (maxsize && newsize > maxsize)
        return 1;

      if (ivtable_expand(newsize, T) != 0)
        return 1;
    }

  iv_t *iv = T->iv;

  for (size_t i = 0; i < active; i++)
    {
      size_t
	j = i + active,
	k = j + active;
      long
	mp = iv[i].mid;

      iv[j].mid = mp + width;
      iv[k].mid = mp - width;
    }

  T->width = width;
  T->active *= 3;

  return 0;
}
