#ifndef ROUND3_H
#define ROUND3_H

#include <stddef.h>

size_t round3(size_t);

#endif
