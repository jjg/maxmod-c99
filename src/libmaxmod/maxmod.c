#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "maxmod.h"

#include "poly.h"
#include "ivtable.h"
#include "round3.h"

#include <math.h>
#include <float.h>

#define IVTABLE_INITIAL 3

/*
  detailed interface

  p   : coefficients of polynomial (n+1 of them)
  n   : order of polynomial
  opt : options struct
  R   : results
*/

int maxmod_d(const double complex *p, size_t n,
             maxmod_opt_t opt,
             maxmod_results_t *R)
{
  if (n < 2)
    {
      R->maxmod = poly_l1norm(p, n);
      R->evaluations = 0;
      R->relerror = n * DBL_EPSILON;
      R->width = 2 * M_PI;

      return MAXMOD_OK;
    }

  double complex pc[n + 1];

  if (poly_conj(p, n, pc) != 0)
    return MAXMOD_POLY;

  double complex p2[2 * n + 1];

  if (poly_mult(p, n, pc, n, p2) != 0)
    return MAXMOD_POLY;

  double
    beta0 = p2[n],
    normq = poly_l1norm(p2, 2 * n),
    lb, ub;

  int err = MAXMOD_OK;

  if (normq - beta0 < 4 * opt.relerror * beta0)
    {
      lb = beta0;
      ub = normq;

      R->evaluations = 0;
      R->width = 2 * M_PI;
    }
  else
    {
      double A = fmax(0, 2 * beta0 - normq);
      ivtable_t *T;

      if ((T = ivtable_new()) == NULL)
	return MAXMOD_MALLOC;

      size_t initial = round3(n * 2 + 1);

      ivtable_init(IVTABLE_INITIAL * initial, initial, T);
      ivtable_eval(p, n, T, 0);

      size_t
	evaluated = ivtable_evaluated(T),
	total_evals = evaluated;

      while (1)
	{
	  lb = ivtable_largest(T);

	  double K = cos(n * ivtable_width(T) * ivtable_delta(T) / 2);

	  ub = (lb - A) / K + A;

	  if (ub < (4 * opt.relerror + 1) * lb)
	    break;

	  size_t rejected = 0;

	  ivtable_reject(T, (lb - A) * K + A, &rejected);

	  if (ivtable_subdivide(T, opt.max.intervals) != 0)
	    {
	      err = MAXMOD_INACCURATE;
	      break;
	    }

	  if (ivtable_eval(p, n, T, opt.max.evaluations) != 0)
	    {
	      err = MAXMOD_INACCURATE;
	      break;
	    }

	  evaluated = ivtable_evaluated(T);
	  total_evals += evaluated;
	}

      R->evaluations = total_evals;
      R->width = (double) ivtable_width(T) * ivtable_delta(T);

      ivtable_destroy(T);
    }

  double
    sqrtub = sqrt(ub),
    sqrtlb = sqrt(lb);

  R->maxmod = (sqrtub + sqrtlb) / 2;
  R->relerror = (sqrtub - sqrtlb) / (2 * R->maxmod);

  return err;
}

/* simplified interface */

double maxmod(const double complex *p, size_t n)
{
  maxmod_opt_t opt = {
    .relerror = DBL_EPSILON * 100,
    .max = {
      .evaluations = 0,
      .intervals = 0
    }
  };
  maxmod_results_t R = {0};

  maxmod_d(p, n, opt, &R);

  return R.maxmod;
}
