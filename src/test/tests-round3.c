#include <round3.h>

#include "tests-round3.h"

CU_TestInfo tests_round3[] = {
  {"round3", test_round3_round3},
  CU_TEST_INFO_NULL,
};

void test_round3_round3(void)
{
  CU_ASSERT_EQUAL(round3(0), 3);
  CU_ASSERT_EQUAL(round3(1), 3);
  CU_ASSERT_EQUAL(round3(2), 3);
  CU_ASSERT_EQUAL(round3(4), 9);
  CU_ASSERT_EQUAL(round3(9), 9);
  CU_ASSERT_EQUAL(round3(10), 27);
}
