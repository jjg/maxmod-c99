#include <maxmod.h>

#include <float.h>
#include <complex.h>

#include "tests-maxmod.h"

CU_TestInfo tests_maxmod[] = {
  {"constant", test_maxmod_constant},
  {"linear", test_maxmod_linear},
  {"unit coefficient", test_maxmod_unit_coefficient},
  {"plateau", test_maxmod_plateau},
  {"small perturbation", test_maxmod_small_perturbation},
  {"monomial short-circuit", test_maxmod_monomial_short_circuit},
  {"simplified interface", test_maxmod_simplified},
  {"Rudin-Shapiro", test_maxmod_rudin_shapiro},
  CU_TEST_INFO_NULL,
};

/* maximal precision (decimal) */

#define PREC 14

/*
  harness for polynomials where the maximum modulus is
  known, we check that the algorithm obtains the value
  to the required precision for a range of precisions,
  and correctly reports this
*/

static double relative_error(double value, double estimate)
{
  return fabs(value - estimate) / value;
}

static void test_mmmerror(double complex* c, size_t n,
			  double true_maxmod,
			  double max_error)
{
  maxmod_results_t R = {0};
  maxmod_opt_t opt = {
    .relerror = max_error,
    .max = {
      .evaluations = 0,
      .intervals = 100*n
    }
  };

  CU_ASSERT(maxmod_d(c, n, opt, &R) == MAXMOD_OK);
  CU_ASSERT(relative_error(true_maxmod, R.maxmod) < max_error);
  CU_ASSERT(R.relerror < max_error);
}

static void test_mm(double complex* c, size_t n, double true_maxmod)
{
  double max_error;

  for (size_t k = n ; k <= PREC ; k++)
    {
      max_error = 1 / pow(10, k);
      test_mmmerror(c, n, true_maxmod, max_error);
    }
}

void test_maxmod_constant(void)
{
  double complex p[1] = {1I};
  test_mm(p, 0, 1);
}

void test_maxmod_linear(void)
{
  double complex p[2] = {1I, 2};
  test_mm(p, 1, 3);
}

/* unit coefficient polynomials */

static void unit_coeff_poly(double complex* c, size_t n)
{
  for (size_t i = 0 ; i <= n ; i++)
    c[i] = 1;
}

void test_maxmod_unit_coefficient(void)
{
  for (size_t n = 3 ; n < 50 ; n++)
    {
      double complex c[n + 1];
      unit_coeff_poly(c, n);
      test_mm(c, n, n + 1);
    }
}

/* small perturbation */

static void small_pert_poly(double complex* c, size_t n)
{
  c[0] = c[1] = 1;

  for (size_t i = 2 ; i < n ; i++)
    c[i] = 0.0;

  c[n] = 0.125;
}

void test_maxmod_small_perturbation(void)
{
  double true_maxmod = 2.125;

  for (size_t n = 3 ; n < 50 ; n++)
    {
      double complex c[n + 1];
      small_pert_poly(c, n);
      test_mm(c, n, true_maxmod);
    }
}

/* plateau polynomials, hard maxmod, check as hard as expected  */

static void plateau_poly(double complex *c, size_t n)
{
  if (n % 4 != 2)
    CU_FAIL("wrong order for plateau polynomial\n");

  for (size_t i = 0 ; i <= n ; i++)
    c[i] = 0;

  c[n / 2] = 0.75;
  c[n / 2 + 2] = c[n / 2 - 2] = 0.125;

  for (size_t i = 1 ; i <= n / 2 ; i += 2)
    {
      double complex z = 2 / (M_PI * I * i * (i * i - 4));

      c[n / 2 + i] = z;
      c[n / 2 - i] = -z;
    }
}

void test_maxmod_plateau(void)
{
  for (size_t n = 5 ; n < 50 ; n += 2)
    {
      double complex c[2 * n + 1];
      maxmod_results_t R = {0};
      maxmod_opt_t opt = {
	.relerror = 40.0 / (3.0 * M_PI * n * n),
	.max = {
	  .evaluations = 0,
	  .intervals = 0,
	}
      };
      plateau_poly(c, 2 * n);
      maxmod_d(c, 2 * n, opt, &R);
      CU_ASSERT(R.evaluations > ceil(0.88 * n * n / 3.0));
    }
}

/* monomials are a special case (and should short-circuit) */

static void monomial_poly(double complex *c, size_t n)
{
  c[0] = 1;
  for (size_t i = 1 ; i <= n ; i++)
    c[i] = 0;
}

void test_maxmod_monomial_short_circuit(void)
{
  for (size_t n = 3 ; n < 10 ; n++)
    {
      double complex c[n + 1];
      maxmod_results_t R = {0};
      maxmod_opt_t opt = {
	.relerror = 1e-12,
	.max = {
	  .evaluations = 0,
	  .intervals   = 0
	}
      };
      monomial_poly(c, n);
      maxmod_d(c, n, opt, &R);
      CU_ASSERT_EQUAL(R.evaluations, 0);
    }
}

/* kick-the-wheels test of the simplified interface */

void test_maxmod_simplified(void)
{
  size_t n = 10;
  double complex c[n + 1];

  unit_coeff_poly(c, n);

  double R = maxmod(c, n);

  CU_ASSERT(relative_error(n+1, R) < 1 / pow(10, PREC - 1));
}

void test_maxmod_rudin_shapiro(void)
{
  size_t M = 12, N = 1 << M;
  double complex p[N], q[N];
  double eps = 3e-11;

  p[0] = q[0] = 1;

  for (size_t m = 1, n = 1 ; m <= M ; m++, n *= 2)
    {
      for (size_t i = 0 ; i < n ; i++)
        {
          p[n + i] = q[i];
          q[n + i] = -q[i];
          q[i] = p[i];
        }

      size_t order = 2 * n - 1;

      maxmod_opt_t opt = {
        .relerror = eps,
        .max = {
          .evaluations = 0,
          .intervals = 0
        }
      };
      maxmod_results_t R;
      int err = maxmod_d(p, order, opt, &R);

      CU_ASSERT_EQUAL(err, MAXMOD_OK);

      double
        d = R.maxmod,
        mu = sqrt(order + 1);

      /*
        Rudin-Shapiro says the d / mu <= sqrt(2), but we have
        only aked for d with relative error eps, so we can
        assert that, in the absence of error in the code, ...
      */

      CU_ASSERT(d / mu < sqrt(2) * (1 + eps));
    }
}
