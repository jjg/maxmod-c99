/*
  tests.c
  testcase loader
  J.J.Green 2015
*/

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include "tests-poly.h"
#include "tests-round3.h"
#include "tests-ivtable.h"
#include "tests-maxmod.h"

#include <CUnit/CUnit.h>

#include "cunit-compat.h"

#define ENTRY(a, b) CU_Suite_Entry((a), NULL, NULL, (b))

static CU_SuiteInfo suites[] =
  {
    ENTRY("poly", tests_poly),
    ENTRY("triadic power", tests_round3),
    ENTRY("interval table", tests_ivtable),
    ENTRY("maxmod", tests_maxmod),
    CU_SUITE_INFO_NULL,
  };

void tests_load(void)
{
  assert(NULL != CU_get_registry());
  assert(!CU_is_test_running());

  if (CU_register_suites(suites) != CUE_SUCCESS)
    {
      fprintf(stderr, "suite registration failed - %s\n",
	      CU_get_error_msg());
      exit(EXIT_FAILURE);
    }
}
