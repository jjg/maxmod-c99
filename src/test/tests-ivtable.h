#include <CUnit/CUnit.h>

extern CU_TestInfo tests_ivtable[];

void test_ivtable_create(void);
void test_ivtable_destroy_null(void);
void test_ivtable_init(void);
void test_ivtable_eval_nomax(void);
void test_ivtable_eval_max(void);
void test_ivtable_largest(void);
void test_ivtable_reject(void);
void test_ivtable_subdivide(void);
