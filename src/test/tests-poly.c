#include <poly.h>

#include "tests-poly.h"

CU_TestInfo tests_poly[] = {
  {"multiply", test_poly_mult},
  {"conjuate", test_poly_conj},
  {"evaluate", test_poly_eval},
  {"evaluate abs square", test_poly_sqr_abs_eval},
  {"l1-norm", test_poly_l1norm},
  CU_TEST_INFO_NULL,
};

static void assert_relative(double complex a,
			    double complex b,
			    double eps)
{
  CU_ASSERT(cabs(a - b) <= cabs(a) * eps);
}

static void assert_poly(double complex *p1,
			double complex *p2,
			size_t n,
			double eps)
{
  for (size_t i = 0 ; i <= n ; i++)
    assert_relative(p1[i], p2[i], eps);
}

/* 1*1 == 1 */

static void test_poly_mult_constant(void)
{
  double complex p[1] = {1.0}, r[1];

  CU_ASSERT(poly_mult(p, 0, p, 0, r) == 0);
  assert_poly(r, p, 0, 1e-10);
}

/* (i+x)*(1+ix) == i + i x^2 */

static void test_poly_mult_linear(void)
{
  double complex
    p[2] = {1I, 1},
    q[2] = {1, 1I},
    pq[3],
    r[3] = {1I, 0, 1I};

  CU_ASSERT(poly_mult(p, 1, q, 1, pq) == 0);
  assert_poly(pq, r, 2, 1e-10);
}

void test_poly_mult(void)
{
  test_poly_mult_constant();
  test_poly_mult_linear();
}

void test_poly_conj(void)
{
  double complex
    p[4] = {0, 1I, 2, 3 + 4I},
    pc[4],
    r[4] = {3-4I, 2, -I, 0};

  CU_ASSERT(poly_conj(p, 3, pc) == 0);
  assert_poly(pc, r, 3, 1e-10);
}

void test_poly_eval(void)
{
  double complex p[3] = {1, 2I, 3 + 4I};

  assert_relative(poly_eval(p, 2, 0), 1, 1e-10);
  assert_relative(poly_eval(p, 2, 1), 4 + 6I, 1e-10);
  assert_relative(poly_eval(p, 2, -1), 4 + 2I, 1e-10);
  assert_relative(poly_eval(p, 2, I), -4-4I, 1e-10);
  assert_relative(poly_eval(p, 2, -I), -4I, 1e-10);
}

void test_poly_sqr_abs_eval(void)
{
  double complex p[3] = {1, 2I, 3 + 4I};

  assert_relative(poly_sqr_abs_eval(p, 2, 0), 1, 1e-10);
  assert_relative(poly_sqr_abs_eval(p, 2, 1), 52, 1e-10);
  assert_relative(poly_sqr_abs_eval(p, 2, -1), 20, 1e-10);
  assert_relative(poly_sqr_abs_eval(p, 2, I), 32, 1e-10);
  assert_relative(poly_sqr_abs_eval(p, 2, -I), 16, 1e-10);
}

void test_poly_l1norm(void)
{
 double complex p[3] = {1, 2I, 3 + 4I};

 assert_relative(poly_l1norm(p, 2), 8, 1e-10);
}
