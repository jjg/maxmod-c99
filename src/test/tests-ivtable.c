#include <ivtable.h>
#include <round3.h>

#include "tests-ivtable.h"

CU_TestInfo tests_ivtable[] = {
  {"create", test_ivtable_create},
  {"destroy a NULL", test_ivtable_destroy_null},
  {"initialise", test_ivtable_init},
  {"evaluate without max", test_ivtable_eval_nomax},
  {"evaluate with max", test_ivtable_eval_max},
  {"find largest value", test_ivtable_largest},
  {"reject", test_ivtable_reject},
  {"subdivide", test_ivtable_subdivide},
  CU_TEST_INFO_NULL,
};

void test_ivtable_create(void)
{
  ivtable_t *t = ivtable_new();
  CU_ASSERT_PTR_NOT_NULL(t);
  ivtable_destroy(t);
}

void test_ivtable_destroy_null(void)
{
  ivtable_destroy(NULL);
}

static void assert_init_reasonable(ivtable_t *t)
{
  CU_ASSERT_EQUAL(ivtable_evaluated(t), 0);
  CU_ASSERT(ivtable_delta(t) > 0.0);
  CU_ASSERT(ivtable_width(t) > 0);

  double D = ivtable_delta(t) * ivtable_width(t);

  CU_ASSERT(D < 2 * M_PI);
}

static void test_ivtable_init_zero(ivtable_t *t)
{
  CU_ASSERT_EQUAL(ivtable_init(0, 0, t), 0);
  assert_init_reasonable(t);
}

static void test_ivtable_init_sz_gt_in(ivtable_t *t)
{
  CU_ASSERT_EQUAL(ivtable_init(1000, 0, t), 0);
  assert_init_reasonable(t);
}

static void test_ivtable_init_n(ivtable_t *t)
{
  int n[4] = {1, 3, 8, 100};

  for (size_t i = 0 ; i < 4 ; i++)
    {
      CU_ASSERT_EQUAL(ivtable_init(0, n[i], t), 0);
      assert_init_reasonable(t);
    }
}

void test_ivtable_init(void)
{
  ivtable_t *t = ivtable_new();
  CU_ASSERT_PTR_NOT_NULL_FATAL(t);

  test_ivtable_init_zero(t);
  test_ivtable_init_sz_gt_in(t);
  test_ivtable_init_n(t);

  ivtable_destroy(t);
}

void test_ivtable_eval_nomax(void)
{
  ivtable_t *t = ivtable_new();
  CU_ASSERT_PTR_NOT_NULL_FATAL(t);
  CU_ASSERT_EQUAL_FATAL(ivtable_init(0, 0, t), 0);

  double complex p[3] = {1, I, 1 + I};

  CU_ASSERT_EQUAL_FATAL(ivtable_evaluated(t), 0);
  CU_ASSERT_EQUAL(ivtable_eval(p, 2, t, 0), 0);
  CU_ASSERT_NOT_EQUAL(ivtable_evaluated(t), 0);

  ivtable_destroy(t);
}

void test_ivtable_eval_max(void)
{
  ivtable_t *t = ivtable_new();
  CU_ASSERT_PTR_NOT_NULL_FATAL(t);
  CU_ASSERT_EQUAL_FATAL(ivtable_init(0, 0, t), 0);

  double complex p[3] = {1, I, 1 + I};

  CU_ASSERT_EQUAL_FATAL(ivtable_evaluated(t), 0);
  CU_ASSERT_NOT_EQUAL(ivtable_eval(p, 2, t, 1), 0);
  CU_ASSERT_EQUAL(ivtable_evaluated(t), 0);

  ivtable_destroy(t);
}

void test_ivtable_largest(void)
{
  ivtable_t *t = ivtable_new();
  CU_ASSERT_PTR_NOT_NULL_FATAL(t);
  CU_ASSERT_EQUAL_FATAL(ivtable_init(0, 0, t), 0);

  /* initial value should be zero */

  CU_ASSERT_EQUAL(ivtable_largest(t), 0);

  /* maximum modulus is 2 */

  double complex p[3] = {1, 0, I};

  CU_ASSERT_EQUAL_FATAL(ivtable_eval(p, 2, t, 0), 0);

  /* ensure no short-circuit */

  CU_ASSERT_FATAL(ivtable_evaluated(t) > 0);

  double M = ivtable_largest(t);
  CU_ASSERT(M >= 0.0);
  CU_ASSERT(M <= 4.0);

  ivtable_destroy(t);
}

void test_ivtable_reject(void)
{
  ivtable_t *t = ivtable_new();
  CU_ASSERT_PTR_NOT_NULL_FATAL(t);
  CU_ASSERT_EQUAL_FATAL(ivtable_init(0, 0, t), 0);

  double complex p[3] = {1, 0, I};

  CU_ASSERT_EQUAL_FATAL(ivtable_eval(p, 2, t, 0), 0);

  /* number we start with */

  size_t R, N = ivtable_active(t);

  /* reject below half-height */

  double y = ivtable_largest(t) / 2;

  CU_ASSERT_EQUAL(ivtable_reject(t, y, &R), 0);

  /* some, but not all should be rejected */

  CU_ASSERT(R > 0);
  CU_ASSERT(R < N);

  /* rejected + remaining should be what we started with */

  CU_ASSERT_EQUAL(R + ivtable_active(t), N);

  ivtable_destroy(t);
}

void test_ivtable_subdivide(void)
{
  ivtable_t *t = ivtable_new();
  CU_ASSERT_PTR_NOT_NULL_FATAL(t);
  CU_ASSERT_EQUAL_FATAL(ivtable_init(0, 0, t), 0);

  double complex p[3] = {1, 0, I};

  CU_ASSERT_EQUAL_FATAL(ivtable_eval(p, 2, t, 0), 0);

  size_t N = ivtable_active(t);

  CU_ASSERT_FATAL(N > 0);

  CU_ASSERT_EQUAL(ivtable_subdivide(t, 0), 0);
  CU_ASSERT_EQUAL(ivtable_active(t), 3 * N);

  ivtable_destroy(t);
}
