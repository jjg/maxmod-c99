RUBBISH = *~
CONFFILES = config.cache config.log config.status

all:
	$(MAKE) -C src all

test:
	$(MAKE) -C src test

install:
	$(MAKE) -C src install

clean:
	$(MAKE) -C src clean
	$(RM) $(RUBBISH)

spotless:
	$(MAKE) -C src spotless
	$(RM) $(CONFFILES) $(RUBBISH)
	rm -rf autom4te.cache/

.PHONY: all test install clean spotless

# note that ./configure creates the version file VERSION, the variable
# $(VERSION) reads that file, so one should not use that variable
# until ./configure has been invoked, it will contain the old version.

VERSION = $(shell cat VERSION)

update-autoconf:
	aclocal --output config/aclocal.m4
	autoconf
	autoheader

update-assets:
	./configure
	bin/version-assets
	$(MAKE)
	$(MAKE) spotless

DIST = maxmod-c99-$(VERSION)

dist:
	tar -C .. \
	  --transform s/^c/$(DIST)/ \
	  --exclude-from .distignore \
	  -zpcvf ../$(DIST).tar.gz c

.PHONY: dist
